#!/usr/bin/env python3
#
#   usage:
#       fimo_gff_match_bed.py -gff <gff3> -bed <bedfile>
#
#   description:
#       Forms a new GFF3 file with feature names from <bed> file and attributes
#       from <gff3> file wherein the start and end coordinates are relative to
#       the initial reference sequence. Absolute coordinates of the BED feature
#       in which the motif match occured and the relative coordinates of the
#       match in that feature are given as attributes.
#       
#       The feature name from the gff3 file is assumed to be <chr>:<start>-<end>
#       This is the default naming when using bedtools v2.31.1 to extrac
#       sequences from a reference using a BED file, and the name is carried
#       through by fimo v5.5.5 to the output fimo.gff when using the -no-pgc
#       option.


from getArgs import getArg
from gff3line import GFF3_line
import re


# Command line arguments
gff3filename = getArg("-gff")
bedfilename = getArg("-bed")

# Store map of feature names to end coordinate
# How many fields in the bed file
bedfile = open(bedfilename)
fieldCt = len(next(bedfile).strip().split("\t"))
bedfile.close()

# Dict of end coords to feature names 
if fieldCt >= 4:
    coordFeatnameDict = {linedata.strip().split("\t")[2]:linedata.strip().split("\t")[3] for linedata in open(bedfilename).read().strip().split("\n")}
else:
    coordFeatnameDict = {linedata.strip().split("\t")[2]:linedata.strip().split("\t")[2] for linedata in open(bedfilename).read().strip().split("\n")}

# Parse <gff3> and 
with open(gff3filename) as infl:
    for line in infl:
        if line.startswith("#"):
            continue
        gff3line = GFF3_line(line)
        seq, parentstart, parentend = re.split("[:-]", gff3line.seqid)
        parentstart, parentend = [int(parentstart), int(parentend)]
        localstart, localend = [gff3line.start, gff3line.end]
        featstart, featend = [parentstart + localstart - 1, parentstart + localend - 1]
        # Modify gff3 line
        gff3line.seqid = seq
        gff3line.start = featstart
        gff3line.end = featend
        gff3line.addAttr("local_match_coords", f"{localstart}-{localend}")
        gff3line.addAttr("parent_feat_coords", f"{parentstart}-{parentend}")
        gff3line.addAttr("parent_name", coordFeatnameDict[str(parentend)])
        print(gff3line)
        
        
###gff-version 3
#1:69783-70035	fimo	nucleotide_motif	145	154	48.2	+	.	Name=TCYACCTAMC_1:69783-70035+;Alias=1-TCYACCTAMC,BestGuess:MYB111/MA1036.1/Jaspar(0.963);ID=TCYACCTAMC-1-TCYACCTAMC,BestGuess:MYB111/MA1036.1/Jaspar(0.963)-1-1:69783-70035;pvalue=1.51e-05;qvalue=0.00741;sequence=TTCACCTAAC;
#1:87038-87262	fimo	nucleotide_motif	79	88	50.7	+	.	Name=TCYACCTAMC_1:87038-87262+;Alias=1-TCYACCTAMC,BestGuess:MYB111/MA1036.1/Jaspar(0.963);ID=TCYACCTAMC-1-TCYACCTAMC,BestGuess:MYB111/MA1036.1/Jaspar(0.963)-1-1:87038-87262;pvalue=8.49e-06;qvalue=0.00668;sequence=CGCACCTAAC;
#
#
#1	13824	14257	AtMYB67_peak_1	127	.	4.86034	15.5595	12.7509	148
#1	30207	30456	AtMYB67_peak_2	89	.	4.82525	11.5018	8.9313	89

##!/usr/bin/env python3
#
#
#class GFF3_line:
#    """A class to represet GFF3 lines and allow modification of the
#    values of its fields.
#
#    Attributes:
#    ------------
#    field0, ..., field8   strings containing the values of each field
#                          in a GFF3 line
#    attributes            a dictionary containing the key-value pairs
#                          in the GFF3 line 9th field
#
#    Methods:    
#    ------------
#    str()               Outputs GFF3 line
#    repr()              Outputs GFF3 line
#    addAttr()           Adds new GFF3 attribute
#    delAttr()           Delete a GFF3 attribute
#    refreshAttrStr()    This needs to be called if changes were made to
#                        any of the attributes. It refreshes
#    """
#    def __init__(self, line=None, **kwargs):
#        """GFF3_line is initialized to contain the fields of the GFF3
#        line provided as attributes. The attributes are kept in a
#        dictionary and a the keys are ordered in a list to preserve
#        the order of attributes upon getting the sring representation
#        of the line from the GFF3_line object.
#        """
#        if line == None:
#            (self.seqid, 
#             self.source, 
#             self.type, 
#             self.start, 
#             self.end, 
#             self.score, 
#             self.strand, 
#             self.phase, 
#             self.attributes_str) = [None]*9
#            self.attributes_order = []
#            self.attributes = {}
#        else:
#            (self.seqid, 
#             self.source, 
#             self.type, 
#             self.start, 
#             self.end, 
#             self.score, 
#             self.strand, 



