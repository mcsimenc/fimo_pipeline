#!/usr/bin/env python3
#
#   usage:
#       make_fimo_bg.py <fasta>
#
#   description:
#       Returns the proportion of each nucleotide in the fasta file


import pyfastx as pfx
import sys

# Expected symbols in FASTA <fasta>
bases = ["A", "C", "G", "T"]
# FASTA file path
fastaFile = sys.argv[1]
# Base counts determined
# pyfastx v1.1.0
charComp = pfx.Fasta(fastaFile).composition
# Length of FASTA sequence
fastaLen = sum([charComp[i] for i in charComp if i in bases])
# Base composition as proportion
print("#\torder 0")
for base in bases:
    prop = charComp[base] / fastaLen
    print(f"{base}\t{prop}")
