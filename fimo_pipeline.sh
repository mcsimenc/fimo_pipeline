#!/usr/bin/env bash
#
#   usage:
#       fimo_pipeline.sh <bed> <motif.meme> <fasta> <qval.thresh> <outdir> <outbase>
#
#   description:
#       Given a reference sequence, a BED file with coordinates for target loci,
#       and a PWM file, locate significant sequence matches to the PWMs (a q-value
#       threshold must be provided). An output directory must also be provided.


BEDFILE=$1
MOTIFFILE=$2
SEQFILE=$3
QVALTHRESH=$4
OUTDIR=$5
OUTBASE=$6

# Make output directory if it does not exist
mkdir -p $OUTDIR

# Form name for motif match sequence output file
BEDFASTA=${OUTDIR}/`basename ${BEDFILE%.bed}.fasta`

# Get sequences of loci from input BED file
bedtools getfasta -fi $SEQFILE -bed $BEDFILE -fullHeader > $BEDFASTA

# Calculate nucleotide frequency (assumes only A,T,C,G present) among loci
# sequences to use as background frequency in FIMO
make_fimo_bg.py $BEDFASTA > ${BEDFASTA}.fimo_bgfile

# Use input file (already in meme format)
MEMEMOTIF=$MOTIFFILE

# Run FIMO to identify subsequences matching the PWM
fimo --no-pgc --oc $OUTDIR --bfile ${BEDFASTA}.fimo_bgfile --thresh $QVALTHRESH --qv-thresh --max-strand $MEMEMOTIF $BEDFASTA

# Custom GFF3 file
./fimo_gff_match_bed.py -gff $OUTDIR/fimo.gff -bed $BEDFILE > $OUTDIR/${OUTBASE}.gff3
