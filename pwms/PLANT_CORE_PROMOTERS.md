#### The PWMs
`plant_core_promoters.meme` contains PWMs from Tores et al. 2021 and the Arabidopsis CCAAT PWM from PlantProm DB:
- TATA
- Inr
- BREd
- BREu
- Y Patch
- TCT
___

##### 1. PlantProm DB
http://www.softberry.com/berry.phtml?topic=plantprom&group=data&subgroup=plantprom

Elements:
- TATA box (*eukaryotes)
- Inr (aka TSS) (*plants)
- CCAAT (aka CAAT) *eukaryotes)

Files:
```
PLPR_TATA.h
PLPR_TSS.h
PLPR_CAAT.h
```


##### 2. Tores et al. 2021
https://doi.org/10.1038/s41477-021-00932-y
https://github.com/tobjores/Synthetic-Promoter-Designs-Enabled-by-a-Comprehensive-Analysis-of-Plant-Core-Promoters/blob/main/data/misc/CPEs.meme

Elements:
- TATA box (from PlantProm DB) (*eukaryotes)
- Inr element (from PlantProm DB) (*plants)
- BRE.d and BRE.u (from JASPAR) (*animals)
- Y Patch (compiled from the literature) (*plant-specific?)
- TCT (compiled from the literature) (*animals)

Files:
```
CPEs.Tores_et_al.meme
```

##### 3. JASPAR
BREu: https://jaspar2018.genereg.net/matrix/POL006.1/
BREd: https://jaspar2018.genereg.net/matrix/POL007.1/

Elements:
- BRE motifs (*animals)

Files:
```
Bre.d.POL007.1.jaspar
Bre.d.POL007.1.meme
Bre.d.POL007.1.pfm
Bre.d.POL007.1.transfac
Bre.u.POL006.1.jaspar
Bre.u.POL006.1.meme
Bre.u.POL006.1.pfm
Bre.u.POL006.1.transfac
```