#!/usr/bin/env bash
INDIR=example                                                                      

# Example
#./fimo_pipeline.sh ${INDIR}/peaks.narrowPeak ${INDIR}/motif.meme ${INDIR}/ref_seq.fasta 0.02 results example
./fimo_pipeline.sh ${INDIR}/peaks.bed ${INDIR}/motif.meme ${INDIR}/ref_seq.fasta 0.02 results results
