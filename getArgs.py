import sys

def getArg(arg, default = None, func = None):
    '''Returns the item one index further than the item matching arg in the 
       list sys.argv. If it doesn't exists return default. Apply func to the
       argument if it is specified.'''

    try:
        argRes = sys.argv[sys.argv.index(arg) + 1]
    except ValueError:
        if default == None:
            exit(f"No default for: {arg}")

        argRes = default

    if not func == None:
        argRes = func(argRes)

    return (argRes)
