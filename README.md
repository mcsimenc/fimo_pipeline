## FIMO pipeline

#### Purpose
Given a file containing a set of position-specific weight matrices (PWMs) in MEME format, a reference sequence, and a named (optional) set of intervals with respect to the reference sequence in BED format, locate interval subsequences matching the PWMs and report a summary in GFF3 format.

#### Installation
###### Install dependencies
1. Install mamba (conda)
2. Install dependencies using mamba:  
    `mamba env create --file motif_analysis.conda.yml`
3. Load dependencies:  
    `mamba activate motif_analysis`
###### Download pipeline
1. Clone the fimo_pipeline repository  
    `git clone https://gitlab.com/mcsimenc/fimo_pipeline.git`  
2. Enter the new directory and run the analysis  
    `cd fimo_pipeline`  
    `./fimo_pipeline <arguments...>`  

#### Inputs and instructions
```
usage:
    fimo_pipeline.sh <bed> <motif.meme> <fasta> <qval.thresh> <outdir> <outbase>

arguments::
    <fasta>       Reference sequence in FASTA format.
    <bed>         Intervals with respect to reference sequence in BED format. If
                  the features are unnamed (there is no 4th BED field) then their 
                  coordinates will be used as names in the final GFF3 output.
    <motif.meme>  A position-specific weight matrix (PWM) in MEME format.
    <qval.thresh> A value between 0-1; sequence matches to the PWMs with
                  q-values greater than this will not be reported.
    <outdir>      The output directory. It will be created if it does not exist.
    <outbase>     The prefix for the GFF3 output file.
```

#### Example
**Note:** The reference sequence must be decompressed to run the example:  
`gunzip example/ref_seq.fasta.gz`

```
INDIR=example
./bedMotifAnnot.sh ${INDIR}/peaks.bed ${INDIR}/motif.meme ${INDIR}/ref_seq.fasta 0.02 results results
```

#### Output
Matches to the PWMs are summarized in a GFF3 file (below, results.gff3) with coordinates in respect to the reference sequence, and FIMO reporting: the motif name, the matched subsequence, and statistical hypothesis test results, as well as the parent interval name (or coordinates, if unnamed), its coordinates with respect to the reference sequence, and subsequence match coordinates with respect to the interval.
    
For example:  
```
1	fimo	nucleotide_motif	30342	30351	40.1	+	.	parent_name=AtMYB67_peak_2;parent_feat_coords=30207-30456;local_match_coords=136-145;Name=TCYACCTAMC_1:30207-30456+;Alias=1-TCYACCTAMC,BestGuess:MYB111/MA1036.1/Jaspar(0.963);ID=TCYACCTAMC-1-TCYACCTAMC,BestGuess:MYB111/MA1036.1/Jaspar(0.963)-1-1:30207-30456;pvalue=9.69e-05;qvalue=0.0164;sequence=TCTAACTAAC
```

The output directory contains additional output from FIMO and the nucleotide sequence frequencies used as the background model for its statistical hypothesis testing method:
```
results/
├── best_site.narrowPeak
├── cisml.xml
├── fimo.gff
├── fimo.html
├── fimo.tsv
├── fimo.xml
├── peaks.fasta
├── peaks.fasta.fimo_bgfile
├── peaks.fasta.fxi
└── results.gff3
```

#### One way to prepare for the analysis of gene promoter regions from gene features in GFF3 or GTF format
 1. Obtain genes of interest from Arabidopsis GFF3 or GTF, for example:
    `grep "\tgene\t" Gene_annotation.gff3 | grep AT3G42670 > CLSY1.gff3`
 2. Calculate promoter coordinates using 0-based indexing for BED file
   2a. Genes on plus strand
         promoter.start = gene.start - 1 - promoter.length
         promoter.end   = gene.start - 1
   2b. Genes on minus strand
         promoter.start = gene.end
         promoter.end = gene.end + promoter.length
 3. Create tab-delimited text file with the following fields (this is BED format):
    This can be done in a spreadsheet program like MS Excel or LibreOffice Calc
    where an option to save as CSV format may be present, and not TSV, but upon
    choosing this format the option to select a delimiter is given.
       1. sequence name (chromosome name)
       2. promoter start position
       3. promoter end position
       4. name (e.g. gene name)
